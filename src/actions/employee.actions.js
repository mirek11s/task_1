import {
    ADD_EMPLOYEE_ERROR,
    ADD_EMPLOYEE_LOADING,
    ADD_EMPLOYEE_SUCCESS,
    DELETE_EMPLOYEE_ERROR,
    DELETE_EMPLOYEE_LOADING,
    DELETE_EMPLOYEE_SUCCESS,
    EDIT_EMPLOYEE_ERROR,
    EDIT_EMPLOYEE_LOADING,
    EDIT_EMPLOYEE_SUCCESS,
    FETCH_EMPLOYEE_ERROR,
    FETCH_EMPLOYEE_LOADING,
    FETCH_EMPLOYEE_SUCCESS
} from './types';

import { employeeData } from '../data';

export const fetchEmployeesSuccess = (data) => {
    return {
        type: FETCH_EMPLOYEE_SUCCESS,
        payload: data,
    }
}

export const fetchEmployees = () => {
    return (dispatch) => {
        dispatch(fetchEmployeesSuccess(employeeData));
    }
};