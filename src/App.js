import './App.css';
import { BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import Nav from './components/Nav';
import React, {Component} from 'react';
import Employees from './containers/Employee';

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      pathname: '',
    };

    this.notifyPathname = this.notifyPathname.bind(this);
  }

  // pass this func into the nav component
  // and the Nav component will notify the App component about the current route we are in
  notifyPathname(pathname) {
    this.setState({
      pathname: pathname,
    });
  }

  render() {
    return (
      <Router>
        <div className="App">
          {/* add components and page names here */}
          {/* Nav renders link elements */}
          <Switch>
            <Route path="/"
                   exact
                   component={() => <Employees />}
            />
          </Switch>
        </div>
       </Router>
    )
  }
}

export default App;

