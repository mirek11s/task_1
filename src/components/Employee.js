import React, {Component} from "react";
import './Employee.css';
import EditEmployee from "../containers/EditEmployee";

//CHILD

class Employee extends Component {
    constructor(props) {
        super(props);
        this.state = {
            renderView:false, id: 0,employee: {}
          };
        this.editClicker = this.editClicker.bind(this);
    }

    // will take the data from the id employee we click and parse it further
    editClicker = (id,employee) => {
        console.log(id)
        this.setState({renderView: true, id:id, employee:employee});
    }

    render() {
        const employee = this.props.employeeData
        if(this.state.renderView){
            return(
                <EditEmployee id={this.state.id} employee={this.state.employee} />
            )
        } else {
        return (
            <div>
                <article className = 'employee' onClick={() => this.editClicker(employee.id,employee)}>
                    <h1>{employee.name}</h1>
                    <h4>{employee.email}</h4>
                    <h4>{employee.address['city']}</h4>
                    <h4>{employee.phone}</h4>
                    <h4>{employee.website}</h4>
                    <h4>{employee.company['name']}</h4>    
              </article>
              
            </div>
        )
    }
    }
}



export default Employee;

