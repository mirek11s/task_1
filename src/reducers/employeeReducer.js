import {
    ADD_EMPLOYEE_ERROR,
    ADD_EMPLOYEE_LOADING,
    ADD_EMPLOYEE_SUCCESS,
    DELETE_EMPLOYEE_ERROR,
    DELETE_EMPLOYEE_LOADING,
    DELETE_EMPLOYEE_SUCCESS,
    EDIT_EMPLOYEE_ERROR,
    EDIT_EMPLOYEE_LOADING,
    EDIT_EMPLOYEE_SUCCESS,
    FETCH_EMPLOYEE_ERROR,
    FETCH_EMPLOYEE_LOADING,
    FETCH_EMPLOYEE_SUCCESS
} from '../actions/types';

const defaultState = {
    employees: [],
    error: null,
    isLoading: false,
};

const employeeReducer = (state = defaultState, action) => {
    switch(action.type) {
        case FETCH_EMPLOYEE_SUCCESS:
            return {...state, employees: action.payload};
        default: 
            return state;
    }
}

export default employeeReducer;