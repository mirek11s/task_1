import { combineReducers } from "redux";
import employees from './employeeReducer';


// if you have more components'reducers here we combine them
export default combineReducers({
    employeesData: employees,
});

