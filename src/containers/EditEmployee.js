import React, { Component } from 'react';
import './EditEmployee.css'


class EditEmployee extends Component {
    constructor(props){
        super(props);
    }
    


    render() {
        const employee_data = this.props.employee
        console.log(employee_data)
        return (
            <div className="create-book">
                <form>
                    <div className="form-group">
                        <input
                            type="text"
                            className="form-control"
                            name="name"
                            placeholder="Enter Name"
                            value={employee_data.name}
                        />
                    </div>
                    <div className="form-group">
                    <input
                            type="text"
                            className="form-control"
                            name="email"
                            placeholder="Enter Email"
                            value={employee_data.email}
                        />
                    </div>
                    <div className="form-group">
                    <input
                            type="text"
                            className="form-control"
                            name="city_address"
                            placeholder="Enter City"
                            value={employee_data.address['city']}
                        />
                    </div>
                    <div className="form-group">
                    <input
                            type="text"
                            className="form-control"
                            name="phone"
                            placeholder="Enter Phone Number"
                            value={employee_data.phone}
                        />
                    </div>
                    <div className="form-group">
                    <input
                            type="text"
                            className="form-control"
                            name="website"
                            placeholder="Enter Website"
                            value={employee_data.website}
                        />
                    </div>
                    <div className="form-group">
                    <input
                            type="text"
                            className="form-control"
                            name="company"
                            placeholder="Enter Company Name"
                            value={employee_data.company['name']}
                        />
                    </div>                    
                    <div className="form-group">
                        <button type="submit" className="btn btn-primary">
                            Save
                        </button>
                        <button href="/" className="btn btn-primary">
                            Cancel
                        </button>
                    </div>
                </form>
            </div>
        )
    }
}

export default EditEmployee;