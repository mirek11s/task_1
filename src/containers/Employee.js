import React, {Component} from 'react';
import {employeeData} from '../data';
import Employee from "../components/Employee";

import { connect } from 'react-redux';

// parent componenent of Main
class Employees extends Component {
    constructor(props) {
        super(props);
    }

        
    render() {
        return (
            <div className='employeelist'>
            {
                employeeData.map(data => {
                                return (
                                    //return ./components/Book.js
                                    <Employee key={data.id} employeeData={data}/>
                                )
                            })
            }
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        employeeData: state.employeesData.employeeData || [],
    };
};

export default connect(mapStateToProps, null)(Employees);